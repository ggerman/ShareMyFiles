package render

import (
  "io/ioutil"
  "log"
)

func returnHtmlFile (filename string) string {
  file, err := ioutil.ReadFile(filename)  
  if err != nil {
    log.Fatal(err)
  }

  return string(file)
}

func Login () string {
  return returnHtmlFile("tpl/login.tpl.html")
}

func Error () string {
  return returnHtmlFile("tpl/error.tpl.html")
}

func Files() string {
  return returnHtmlFile("The Cranberries - 01 - Hollywood.mp3")  
}

/**
func Logged(username string, password string) {
  return database.Connect(username, password)
}
*/
