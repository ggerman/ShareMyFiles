package server

import (
	"os"
	"path/filepath"
)

type ServedNode struct {
	Uri  string
	Name string
}

func ServedNodeNew(uri string) (node *ServedNode, err error) {

	fileinfo, err := os.Stat(uri) // Check if file exists and is readable
	
	if err != nil {
		return nil, err
	}
	fullpath, err := filepath.Abs(uri)

	node = &ServedNode{Uri: fullpath, Name: fileinfo.Name()}

	if err != nil {
		panic(err)
	}

	return
}


