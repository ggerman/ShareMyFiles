package main

import (
	"github.com/ggerman/ShareMyFiles/lib/ui"
	"github.com/conformal/gotk3/gtk"
)

func main() {
	gtk.Init(nil)
	window := ui.MainWindowNew()

	window.Window.ShowAll()

	gtk.Main()

}
