package rendernc

import (
  gc "code.google.com/p/goncurses"
)

func WAboutus() {
  stdscr, _ := gc.Init();
  defer gc.End()

  gc.StartColor()
  gc.Raw(true)
  gc.Echo(false)
  gc.Cursor(0)

  gc.InitPair(2, gc.C_WHITE, gc.C_BLUE)
  stdscr.SetBackground(gc.Char(' ') | gc.ColorPair(2))                                  
  stdscr.Keypad(true)

  gc.InitPair(1, gc.C_WHITE, gc.C_BLUE)

  menuwin, _ := gc.NewWindow(10, 40, 4, 14)
  menuwin.Keypad(true)

  // Print centered menu title
  _, x := menuwin.MaxYX()
  title := "About Us"
  menuwin.Box(0, 0)
  menuwin.ColorOn(1)
  menuwin.MovePrint(1, (x/2)-(len(title)/2), title)
  menuwin.ColorOff(1)

  menuwin.MovePrint(3, 1, "Los muchachos de EOSWEB")
  menuwin.MovePrint(4, 1, "Estamos en Paraná - Entre Ríos")
  menuwin.MovePrint(5, 1, "Sede mundial de la incertidumbre.")

  menuwin.MoveAddChar(2, 0, gc.ACS_LTEE)
  menuwin.HLine(2, 1, gc.ACS_HLINE, x-3)
  menuwin.MoveAddChar(2, x-2, gc.ACS_RTEE)

  _, x = stdscr.MaxYX()
  stdscr.Refresh()
  menuwin.Refresh()

  stdscr.Print("About Us")
  stdscr.GetChar()
}
