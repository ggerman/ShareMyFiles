package main

import (
	"github.com/ggerman/ShareMyFiles/lib/server"
	"log"
	"os"
)

func main() {
	log.Println("Starting ShareMyFiles")
	ServeMyFiles := server.ServerNew()
	for idx, val := range os.Args {
		if idx != 0 {
			ServeMyFiles.AddFile(val)
		}
	}
	ServeMyFiles.Serve()
}
